<!DOCTYPE html PUBLIC 
	"-//W3C//DTD XHTML 1.1 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><decorator:title default="Struts Starter"/></title>
    <link href="<s:url value='/css/bootstrap.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/css/open-iconic-bootstrap.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="https://fonts.googleapis.com/css?family=Arvo|Open+Sans" rel="stylesheet"/>
    <decorator:head/>
</head>
<body>
<s:if test="%{#session.User != null}">
    <div class="container-fluid mb-3">
        <nav class="navbar">
            <a class="navbar-brand" href="#"><strong>Library Manager</strong></a>
            <ul class="nav nav-pills">
                <li class="nav-item d-none d-md-block">
                    <a class="nav-link" href="<s:url action="../book/search.action"/>" role="button"><span class="oi oi-magnifying-glass"></span></a>
                </li>
                <li class="nav-item dropdown d-none d-md-block">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="oi oi-person"></span></a>
                    <div class="dropdown-menu dropdown-menu-right text-right">
                        <a class="dropdown-item" href="<s:url action="../client/show.action"/>">profil</a>
                        <a class="dropdown-item" href="<s:url action="../borrowing/show.action"/>">Ouvrage(s) emprunté(s)</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<s:url action="../security/logout.action"/>">Déconnexion</a>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler d-sm-block d-md-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto menu-sm">
                    <li class="nav-item">
                        <a class="nav-link" href="<s:url action="../client/show.action"/>">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<s:url action="../borrowing/show.action"/>">Ouvrage(s) emprunté(s)</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<s:url action="../security/logout.action"/>">Déconnexion</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</s:if>
    <decorator:body/>

    <script language="JavaScript" type="text/javascript" src="<s:url value='/js/jquery-3.2.1.min.js'/>"></script>
    <script src=<s:url value="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"/> integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script language="JavaScript" type="text/javascript" src="<s:url value='/js/bootstrap.js'/>"></script>
</body>
</html>
