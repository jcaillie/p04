<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Resultat de votre recherche</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <p><strong>Résultat pour:
                    <ul class="mb-3">
                        <li>Titre: <s:property value="title"/></li>
                        <li>Auteur: <s:property value="author"/></li>
                        <li>Numéro ISBN: <s:property value="isbn"/></li>
                    </ul>
                </strong></p>
            </div>
        </div>
        <div class="row">
            <s:iterator value="books">
            <div class="col-md-6">
                <hr>
                <div class="row">
                    <div class="d-none d-lg-block col-lg-4">
                        <img src="../images/couverture-livre.jpg" class="img-fluid" alt="Responsive image">
                    </div>
                    <div class="col-12 col-lg-8">
                        <h4><s:property value="title"/></h4>
                        <p><strong><s:property value="author"/></strong></p>
                        <p><s:property value="numberAvailable"/> livre(s) disponible</p>
                        <button class="btn" data-toggle="collapse" href="#<s:property value="title"/>" role="button" aria-expanded="false" aria-controls="<s:property value="title"/>">Description</button>
                        <p class="collapse text-justify"id="<s:property value="title"/>"><s:property value="description"/></p>
                    </div>
                </div>
            </div>
            </s:iterator>
        </div>
    </div>
</body>
