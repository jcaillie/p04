<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<title>Recherche d'ouvrage</title>

<body>
    <div class="container-fluid mt-5">
        <form method="post" action="<s:url action="result.action"/>">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 mb-4">
                    <h1>Rechercher un ouvrage</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 form-group">
                    <input name="title" class="col-12 rounded" type="text" id="title" placeholder="Titre ou extrait de titre" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 form-group">
                    <input name="author" class="col-12 rounded" type="text" id="author" placeholder="auteur" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 form-group">
                    <input name="isbn" class="col-12 rounded" type="text" id="isbn" placeholder="Numéro ISBN" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 form-group">
                    <button class="btn col-6 col-md-3" type="submit"><strong>RECHERCHER</strong></button>
                </div>
            </div>
        </form>
    </div>
</body>
