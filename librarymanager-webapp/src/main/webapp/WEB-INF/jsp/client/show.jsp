<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Votre profil</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3 mt-5">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="../images/myAvatar.png" class="img-fluid rounded-circle" alt="Responsive image">
                    </div>
                    <div class="col-sm-8">
                        <h4 class="mb-3"><strong><s:property value="client.lastname"/> <s:property value="client.firstname"/></strong></h4>
                        <p><strong>Date de naissance: <s:date name="client.birthday.toGregorianCalendar()" format="dd/MM/yyyy"/></strong></p>
                        <p><strong>Mail: <s:property value="client.email"/></strong></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr>
            </div>
        </div>
    </div>
</body>
