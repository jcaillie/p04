<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<title>Connexion</title>

<head>
    <link href="<s:url value='/css/login.css'/>" rel="stylesheet" type="text/css" media="all"/>
</head>
<body>
    <div class="container-fluid">
        <form method="post" action="<s:url action="login.action"/>">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 mb-3 text-center">
                    <h1 class="font-weight-bold">LIBRARY MANAGER</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 offset-lg-5 col-md-12 mt-3 form-group">
                    <s:if test="error != null">
                        <div class="error text-center mb-2"><s:property value="error"/> </div>
                    </s:if>
                    <input class="col-12 rounded font-weight-bold" name="email" type="text" placeholder="Email" required="required" value=<s:property value="email"/>>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 offset-lg-5 col-md-12 form-group">
                    <input class="col-12 rounded font-weight-bold" name="password" type="password" placeholder="Mot de Passe" required="required" value=<s:property value="password"/>>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-12 text-center mt-3">
                    <button class="btn col-6 col-md-3 font-weight-bold" type="submit">CONNEXION</button>
                </div>
            </div>
        </form>
    </div>
</body>
