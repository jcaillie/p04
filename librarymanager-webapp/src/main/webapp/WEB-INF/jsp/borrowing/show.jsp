<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste de vos emprunts</title>
</head>
<body>
    <div class="container mt-3">
        <div class="row">
            <h4 class="col-sm-12 mb-5">Ouvrage(s) emprunté(s)</h4>
        </div>
        <div class="row">
        <s:iterator value="borrowings" var="borrowing">
            <div class="col-sm-12 col-lg-4">
                <h5 class=""><s:property value="books[#borrowing.book].title"/></h5>
                <p><strong><s:property value="books[#borrowing.book].author"/> - <s:date name="books[#borrowing.book].releaseDate.toGregorianCalendar()" format="dd/MM/yyyy"/></strong></p>
                <p><strong>ISBN: <s:property value="books[#borrowing.book].isbn"/></strong></p>
                <p><strong>Date d'emprunt: <s:date name="#borrowing.borrowingDate.toGregorianCalendar()" format="dd/MM/yyyy"/></strong></p>
                <s:url action="extend" var="urlExtend">
                    <s:param name="borrowingId"><s:property value="#borrowing.number"/></s:param>
                </s:url>
                <s:if test="#borrowing.renderedUp != null">
                    <p class="rendered-up">RENDU</p>
                </s:if>
                <s:elseif test="#borrowing.deadLineForRendering.toGregorianCalendar() > currentDate.toGregorianCalendar()">
                    <p class="not-rendered"><strong>A rendre avant: <s:date name="#borrowing.deadLineForRendering.toGregorianCalendar()" format="dd/MM/yyyy"/></strong>
                        <s:if test="!#borrowing.isExtended()"><a class="btn" href="<s:property value="#urlExtend"/>" role="button">Prolonger</a></s:if>
                    </p>

                </s:elseif>
                <s:else>
                    <p class="late"><strong>A rendre avant: <s:date name="#borrowing.deadLineForRendering.toGregorianCalendar()" format="dd/MM/yyyy"/></strong>
                        <s:if test="!#borrowing.isExtended()"><a class="btn" href="<s:property value="#urlExtend"/>" role="button">Prolonger</a></s:if>
                    </p>
                </s:else>
                <hr>
            </div>
        </s:iterator>
        </div>
    </div>
</body>
</html>
