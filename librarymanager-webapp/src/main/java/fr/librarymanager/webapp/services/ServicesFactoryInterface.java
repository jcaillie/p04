package fr.librarymanager.webapp.services;

import fr.librarymanager.webapp.services.bookService.BookService;
import fr.librarymanager.webapp.services.borrowingService.BorrowingService;
import fr.librarymanager.webapp.services.clientService.ClientService;

public interface ServicesFactoryInterface {
    public BookService getBookService();
    public BorrowingService getBorrowingService();
    public ClientService getClientService();
}
