
package fr.librarymanager.webapp.services.clientService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.librarymanager.webapp.services.clientService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetClientByEmailResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "getClientByEmailResponse");
    private final static QName _GetClientByIdResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "getClientByIdResponse");
    private final static QName _ClientServiceException_QNAME = new QName("http://webservice.librarymanager.fr/", "ClientServiceException");
    private final static QName _GetClientByEmail_QNAME = new QName("http://webservice.librarymanager.fr/", "getClientByEmail");
    private final static QName _GetClientById_QNAME = new QName("http://webservice.librarymanager.fr/", "getClientById");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.librarymanager.webapp.services.clientService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetClientByIdResponse }
     * 
     */
    public GetClientByIdResponse createGetClientByIdResponse() {
        return new GetClientByIdResponse();
    }

    /**
     * Create an instance of {@link ClientServiceFault }
     * 
     */
    public ClientServiceFault createClientServiceFault() {
        return new ClientServiceFault();
    }

    /**
     * Create an instance of {@link GetClientByEmailResponse }
     * 
     */
    public GetClientByEmailResponse createGetClientByEmailResponse() {
        return new GetClientByEmailResponse();
    }

    /**
     * Create an instance of {@link GetClientByEmail }
     * 
     */
    public GetClientByEmail createGetClientByEmail() {
        return new GetClientByEmail();
    }

    /**
     * Create an instance of {@link GetClientById }
     * 
     */
    public GetClientById createGetClientById() {
        return new GetClientById();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientByEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getClientByEmailResponse")
    public JAXBElement<GetClientByEmailResponse> createGetClientByEmailResponse(GetClientByEmailResponse value) {
        return new JAXBElement<GetClientByEmailResponse>(_GetClientByEmailResponse_QNAME, GetClientByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getClientByIdResponse")
    public JAXBElement<GetClientByIdResponse> createGetClientByIdResponse(GetClientByIdResponse value) {
        return new JAXBElement<GetClientByIdResponse>(_GetClientByIdResponse_QNAME, GetClientByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "ClientServiceException")
    public JAXBElement<ClientServiceFault> createClientServiceException(ClientServiceFault value) {
        return new JAXBElement<ClientServiceFault>(_ClientServiceException_QNAME, ClientServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientByEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getClientByEmail")
    public JAXBElement<GetClientByEmail> createGetClientByEmail(GetClientByEmail value) {
        return new JAXBElement<GetClientByEmail>(_GetClientByEmail_QNAME, GetClientByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getClientById")
    public JAXBElement<GetClientById> createGetClientById(GetClientById value) {
        return new JAXBElement<GetClientById>(_GetClientById_QNAME, GetClientById.class, null, value);
    }

}
