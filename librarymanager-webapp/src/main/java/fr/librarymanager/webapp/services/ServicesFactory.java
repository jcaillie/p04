package fr.librarymanager.webapp.services;

import fr.librarymanager.webapp.services.bookService.BookService;
import fr.librarymanager.webapp.services.bookService.BookServiceService;
import fr.librarymanager.webapp.services.borrowingService.BorrowingService;
import fr.librarymanager.webapp.services.borrowingService.BorrowingServiceService;
import fr.librarymanager.webapp.services.clientService.ClientService;
import fr.librarymanager.webapp.services.clientService.ClientServiceService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * <b>Factory des webservices de l'application.</b>
 *
 * <p>Permet de centraliser la déclaration et l'instanciation des webservices au moyen de l'IOC de Spring.</p>
 */
@Component
@Configuration
@PropertySource("classpath:application.properties")
public class ServicesFactory implements ServicesFactoryInterface {

    @Value("${webservice.url.client}")
    private String urlClient;
    @Value("${webservice.url.book}")
    private String urlBook;
    @Value("${webservice.url.borrowing}")
    private String urlBorrowing;

    @Override
    public BookService getBookService() {
        BookServiceService bookServiceService = null;
        try {
            bookServiceService = new BookServiceService(new URL(urlBook));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BookService bookService = bookServiceService.getBookServicePort();

        return bookService;
    }

    @Override
    public BorrowingService getBorrowingService() {
        BorrowingServiceService borrowingServiceService = null;
        try {
            borrowingServiceService = new BorrowingServiceService(new URL(urlBorrowing));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BorrowingService borrowingService = borrowingServiceService.getBorrowingServicePort();

        return borrowingService;
    }

    @Override
    public ClientService getClientService() {
        ClientServiceService clientServiceService = null;
        try {
            clientServiceService = new ClientServiceService(new URL(urlClient));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ClientService clientService = clientServiceService.getClientServicePort();

        return clientService;
    }
}
