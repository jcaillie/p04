package fr.librarymanager.webapp.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

public class AuthenticationInterceptor extends AbstractInterceptor {

    public void init(){}

    @Override
    public String intercept(ActionInvocation invocation) throws Exception{
        Map<String, Object> session = invocation.getInvocationContext().getSession();

        if(session.get("User") == null){
            return "authentication";
        }
        else{
            return invocation.invoke();
        }
    }
}
