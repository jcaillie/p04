package fr.librarymanager.webapp.actions.client;

import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.clientService.Client;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class ShowProfileAction extends AbstractAction implements SessionAware {

    Map<String, Object> session;
    Client client = null;

    public String execute(){
        client = (Client) session.get("User");

        return SUCCESS;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public void setSession(Map<String, Object> session){
        this.session = session;
    }
}
