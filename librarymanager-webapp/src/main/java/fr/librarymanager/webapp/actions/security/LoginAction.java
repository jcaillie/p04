package fr.librarymanager.webapp.actions.security;

import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.clientService.Client;
import fr.librarymanager.webapp.services.clientService.ClientServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Map;

/**
 * <b>Action gérant la connexion de l'utilisateur</b>
 */
public class LoginAction extends AbstractAction implements SessionAware {

    private static final Logger log = LogManager.getLogger(LoginAction.class);

    private Map<String, Object> session;
    private Client client = null;
    private String email;
    private String password;
    private String error = null;

    @Override
    public String execute(){

        try {
            // Si aucun client n'est enregistré en Session on récupère le client correspondant au mail
            if (session.get("User") == null) {

                // Si error = null alors on demande le premier affichage du formulaire
                if(email == null){
                    return INPUT;
                }

                this.client = this.getServicesFactory().getClientService().getClientByEmail(email);

                // Si le mot de passe correspond alors on enregistre le client en session
                if(DigestUtils.sha256Hex(password).equals(client.getEncryptedPassword())){
                    this.session.put("User", this.client);

                    log.info("Connexion de l'utilisateur " + this.client.getFirstname() + " " + this.client.getLastname());

                    return SUCCESS;
                }else{
                    this.error = "Mauvais mot de passe";

                    return INPUT;
                }
            }else{
                return "authenticated";
            }
        }
        // Si aucun client ne correspond en base de données on intercepte une exception ClientServiceException
        catch (ClientServiceException e) {
            error = e.getMessage();
            log.error(e.getMessage());
            return ERROR;
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getError() { return error; }

    public void setError(String error) { this.error = error; }

    public void setSession(Map<String, Object> session){
        this.session = session;
    }
}
