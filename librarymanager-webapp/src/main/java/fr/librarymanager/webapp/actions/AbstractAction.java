package fr.librarymanager.webapp.actions;

import com.opensymphony.xwork2.ActionSupport;
import fr.librarymanager.webapp.services.ServicesFactoryInterface;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractAction extends ActionSupport {

    @Autowired
    private ServicesFactoryInterface servicesFactory;

    protected ServicesFactoryInterface getServicesFactory(){
        return this.servicesFactory;
    }
}
