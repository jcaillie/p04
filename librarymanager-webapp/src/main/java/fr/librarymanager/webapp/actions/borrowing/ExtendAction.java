package fr.librarymanager.webapp.actions.borrowing;

import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.borrowingService.Borrowing;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class ExtendAction extends AbstractAction implements SessionAware {

    private static final Logger log = LogManager.getLogger(ExtendAction.class);

    private Map<String, Object> session;
    private Integer borrowingId;

    @Override
    public String execute() throws Exception {
        try{
            this.getServicesFactory().getBorrowingService().extendBorrowing(borrowingId);

            return SUCCESS;
        }catch (Exception exception){
            log.error(exception.getMessage());
            return ERROR;
        }
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Integer getBorrowingId() {
        return borrowingId;
    }

    public void setBorrowingId(Integer borrowing) {
        this.borrowingId = borrowing;
    }
}
