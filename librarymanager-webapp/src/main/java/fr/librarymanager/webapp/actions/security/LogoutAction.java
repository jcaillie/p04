package fr.librarymanager.webapp.actions.security;

import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.clientService.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class LogoutAction extends AbstractAction implements SessionAware {

    private static final Logger log = LogManager.getLogger(LogoutAction.class);

    Map<String, Object> session;

    public String execute(){
        if(this.session.get("User") != null ){
            Client client = (Client) this.session.get("User");
            log.info("Deconnexion de l'utilisateur " + client.getFirstname() + " " + client.getLastname());
            session.remove("User");
        }

        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session){
        this.session = session;
    }
}
