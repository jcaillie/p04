package fr.librarymanager.webapp.actions.book;

import fr.librarymanager.webapp.actions.AbstractAction;

public class SearchAction extends AbstractAction {

    private String title;
    private String author;
    private String isbn;

    public String execute() throws Exception {
        return SUCCESS;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
