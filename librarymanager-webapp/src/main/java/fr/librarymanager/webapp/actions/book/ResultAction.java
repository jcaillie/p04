package fr.librarymanager.webapp.actions.book;

import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.bookService.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ResultAction extends AbstractAction {

    private static final Logger log = LogManager.getLogger(ResultAction.class);

    private String title ="";
    private String author = "";
    private String isbn = "";
    private List<Book> books = new ArrayList<>();

    public String execute() throws Exception {

        try{
            this.books = this.getServicesFactory().getBookService().searchBooks(this.title, this.author, this.isbn);

            return SUCCESS;
        }catch (Exception e){
            log.error(e.getMessage());
            return ERROR;
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
