package fr.librarymanager.webapp.actions.borrowing;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import fr.librarymanager.webapp.actions.AbstractAction;
import fr.librarymanager.webapp.services.bookService.Book;
import fr.librarymanager.webapp.services.bookService.BookServiceException;
import fr.librarymanager.webapp.services.borrowingService.Borrowing;
import fr.librarymanager.webapp.services.borrowingService.BorrowingServiceException;
import fr.librarymanager.webapp.services.clientService.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowAction extends AbstractAction implements SessionAware {

    private static final Logger log = LogManager.getLogger(ShowAction.class);
    private Map<String, Object> session;
    private Map<Integer, Book> books = new HashMap<>();
    private List<Borrowing> borrowings;
    private Client client;
    private String error;
    private XMLGregorianCalendar currentDate = new XMLGregorianCalendarImpl();

    public String execute(){
        currentDate.setDay(LocalDate.now().getDayOfMonth());
        currentDate.setMonth(LocalDate.now().getMonthValue());
        currentDate.setYear(LocalDate.now().getYear());

        this.client = (Client) session.get("User");
        try {
            borrowings = this.getServicesFactory().getBorrowingService().getAllBorrowingByClientId(client.getId());
            for (Borrowing borrowing: borrowings) {
                books.put(borrowing.getBook(), this.getServicesFactory().getBookService().getBookById(borrowing.getBook()));
            }

            return SUCCESS;
        } catch (BorrowingServiceException e) {
            error = e.getMessage();
            log.error(e.getMessage());
            return ERROR;
        } catch (BookServiceException e) {
            error = e.getMessage();
            log.error(e.getMessage());
            return ERROR;
        }
    }

    public Map<Integer, Book> getBooks() {
        return books;
    }

    public void setBooks(Map<Integer, Book> books) {
        this.books = books;
    }

    public List<Borrowing> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(List<Borrowing> borrowings) {
        this.borrowings = borrowings;
    }

    public void setSession(Map<String, Object> session){
        this.session = session;
    }

    public XMLGregorianCalendar getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(XMLGregorianCalendar currentDate) {
        this.currentDate = currentDate;
    }
}
