package fr.librarymanager.business.exception;

import fr.librarymanager.model.Borrowing;

public class BorrowingNotFoundException extends Exception {
    public BorrowingNotFoundException(String message){
        super(message);
    }
}
