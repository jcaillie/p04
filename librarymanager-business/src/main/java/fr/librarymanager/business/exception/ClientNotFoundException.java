package fr.librarymanager.business.exception;

public class ClientNotFoundException extends Exception {
    public ClientNotFoundException(String message){
        super(message);
    }
}
