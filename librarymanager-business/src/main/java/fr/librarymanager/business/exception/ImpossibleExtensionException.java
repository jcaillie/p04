package fr.librarymanager.business.exception;

public class ImpossibleExtensionException extends Exception {
    public ImpossibleExtensionException(String message){
        super(message);
    }
}
