package fr.librarymanager.business.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <b>Factory des managers de beans</b>
 *
 * <p>Factory permettant de fournir les différents managers à l'application.</p>
 *
 * @author Jeremy Caillié
 */
@Component
public class ManagerFactory implements ManagerFactoryInterface {

    @Autowired
    private ClientManagerInterface clientManager;

    @Autowired
    private BookManagerInterface bookManager;

    @Autowired
    private BorrowingManagerInterface borrowingManager;

    public ClientManagerInterface getClientManager() {
        return clientManager;
    }
    public BookManagerInterface getBookManager() {
        return bookManager;
    }
    public BorrowingManagerInterface getBorrowingManager() {
        return this.borrowingManager;
    }

}
