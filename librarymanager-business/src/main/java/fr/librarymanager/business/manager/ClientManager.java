package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.ClientNotFoundException;
import fr.librarymanager.model.Client;
import fr.librarymanager.model.ClientInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ClientManager extends AbstractManager implements ClientManagerInterface {

    private static final Logger log = LogManager.getLogger(ClientManager.class);

    /**
     * <b>Récupère un client par son email</b>
     *
     * <p>Permet de récupérer un client en fonction de son email passé en paramètre.</p>
     *
     * @param email l'email du client recherché
     *
     * @return un bean Client représentant le client recherché.
     */
    public ClientInterface getOneByEmail(String email) throws ClientNotFoundException {
        ClientInterface client = this.getDaoFactory().getClientDao().findOneByEmail(email);

        if(client == null){
            log.info("Recherche d'un client innexistant.");
            throw new ClientNotFoundException("L'utilisateur ayant pour email \""+ email +"\" n'a pas été trouvé");
        }

        return client;
    }

    /**
     * <b>Récupère un client par son id</b>
     *
     * <p>Permet de récupérer un client en fonction de son id passé en paramètre.</p>
     *
     * @param id l'id du client recherché
     *
     * @return un bean Client représentant le client recherché.
     */
    public ClientInterface getOneById(Integer id) throws ClientNotFoundException {
        ClientInterface client = this.getDaoFactory().getClientDao().findOneById(id);

        if(client == null){
            log.info("Recherche d'un client innexistant.");
            throw new ClientNotFoundException("Aucun utilisateur ayant l'id " + id + " n'a été trouvé.");
        }

        return client;
    }
}
