package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.BookNotFoundException;
import fr.librarymanager.model.BookInterface;

import java.util.List;

/**
 * <b>Interface du manager du bean Book</b>
 *
 * <p>Permet de gérer l'abstraction du manager BookManager</p>
 */
public interface BookManagerInterface {
    BookInterface getOneById( Integer id ) throws BookNotFoundException;
    List<BookInterface> search(String title, String author, String isbn) throws BookNotFoundException;
}
