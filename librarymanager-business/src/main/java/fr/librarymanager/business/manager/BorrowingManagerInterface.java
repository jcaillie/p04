package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.*;
import fr.librarymanager.model.BorrowingInterface;

import java.util.List;

/**
 * <b>Interface du manager BorrowingManager</b>
 *
 * <p>Interface permettant de gérer l'abstraction du manager de bean Borrowing.</p>
 *
 * @author Jeremy Caillié
 */
public interface BorrowingManagerInterface {
    List<BorrowingInterface> getAllByClientId(Integer clientId) throws BorrowingNotFoundException;
    List<BorrowingInterface> getAllLate() throws BorrowingNotFoundException;
    BorrowingInterface getOneById(Integer id) throws BorrowingNotFoundException;
    void extend(BorrowingInterface borrowing) throws ImpossibleExtensionException;
    void returnBook(BorrowingInterface borrowing) throws BorrowingNotFoundException;
    void borrow(Integer clientId, Integer bookId) throws ClientNotFoundException, BookNotFoundException, BookUnavailableException;
}
