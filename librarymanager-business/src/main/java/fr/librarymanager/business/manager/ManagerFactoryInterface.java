package fr.librarymanager.business.manager;

public interface ManagerFactoryInterface {
    ClientManagerInterface getClientManager();
    BookManagerInterface getBookManager();
    BorrowingManagerInterface getBorrowingManager();
}
