package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.*;
import fr.librarymanager.model.BookInterface;
import fr.librarymanager.model.Borrowing;
import fr.librarymanager.model.BorrowingInterface;
import fr.librarymanager.model.ClientInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <b>Manager des Beans Borrowing</b>
 *
 * <p>Permet de gérer le code métier lié au Bean Borrowing.</p>
 *
 * @author Jeremy Caillié
 */
@Component
public class BorrowingManager extends AbstractManager implements BorrowingManagerInterface {

    private static final Logger log = LogManager.getLogger(BorrowingManager.class);

    @Value("${borrowing.initial_weeks_period}")
    private Integer initialPeriod;

    @Value("${borrowing.extension_weeks_period}")
    private Integer extensionPeriod;

    /**
     * <b>Récupère la liste des emprunts d'un client</b>
     *
     * <p>Demande la récupération des emprunt d'un client à la DAO en fonction de son id.</p>
     *
     * @param clientId l'id du client dont les emprunt sont recherchés
     *
     * @return la liste des emprunts trouvés
     */
    public List<BorrowingInterface> getAllByClientId(Integer clientId) throws BorrowingNotFoundException {
        List<BorrowingInterface> borrowingList = this.getDaoFactory().getBorrowingDao().findAllByClientId(clientId);

        if(borrowingList == null){
            throw new BorrowingNotFoundException("Aucune réservation lié au client d'id " + clientId + " n'a été trouvé.");
        }

        return borrowingList;
    }

    /**
     * <b>Récupère la liste des emprunts en retards</b>
     *
     * <p>Permet de récupérer toute la liste des emprunts réalisés qui n'ont pas été rendu et dont la date de rendu est dépassée.</p>
     *
     * @return la liste des emprunts correspondant aux critères
     *
     * @throws BorrowingNotFoundException
     */
    public List<BorrowingInterface> getAllLate() throws BorrowingNotFoundException {
        List<BorrowingInterface> borrowingList = this.getDaoFactory().getBorrowingDao().findAllLate();

        if(borrowingList == null) {
            throw new BorrowingNotFoundException("Aucun emprunt en retard n'a été trouvé. Tout semble être à jour.");
        }

        return borrowingList;
    }

    /**
     * <b>Etend le prêt d'un ouvrage.</b>
     *
     * <p>Permet d'étendre la durée du pret d'un ouvrage si celui ci n'a pas déjà été étendu.</p>
     *
     * @param borrowing l'emprunt à étendre
     *
     * @return l'emprunt mise à jour
     *
     * @throws ImpossibleExtensionException
     */
    public void extend(BorrowingInterface borrowing) throws ImpossibleExtensionException {
        BorrowingInterface borrowingExtend;

        if(borrowing.getExtended()){
            throw new ImpossibleExtensionException("La durée de l'emprunt à déjà été étendu.");
        }


        Date newDeadLine = new Date(borrowing.getDeadLineForRendering().getTime() + TimeUnit.DAYS.toMillis(7 * this.extensionPeriod));

        borrowing.setDeadLineForRendering(newDeadLine);

        TransactionTemplate transactionTemplate = new TransactionTemplate(this.getPlatformTransactionManager());

        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                getDaoFactory().getBorrowingDao().update(borrowing);
            }
        });
    }


    /**
     * <b>Récupère un emprunt par son id</b>
     *
     * @param id l'id de l'emprunt recherché
     *
     * @return un Bean Borrowing
     *
     * @throws BorrowingNotFoundException
     */
    public BorrowingInterface getOneById(Integer id) throws BorrowingNotFoundException{

        BorrowingInterface borrowing = this.getDaoFactory().getBorrowingDao().findOneById(id);

        if(borrowing == null){
            throw new BorrowingNotFoundException("Aucun emprunt d'id " + id + " n'a été trouvé.");
        }

        return borrowing;
    }

    /**
     * <b>Permet de valider le rendu d'un ouvrage</b>
     *
     * <p>Met à jour un emprunt en validant le rendu et en incrémentant le nombre d'ouvrage disponible d'un.</p>
     *
     * @param borrowing l'emprunt rendu
     *
     * @throws BorrowingNotFoundException
     */
    public void returnBook(BorrowingInterface borrowing)throws BorrowingNotFoundException{

        BookInterface book;

        try{
            book = this.getDaoFactory().getBookDao().findOneById(borrowing.getBook());

            book.setNumberAvailable(book.getNumberAvailable()+1);

            borrowing.setRenderedUp(new Date());

            TransactionTemplate transactionTemplate = new TransactionTemplate(this.getPlatformTransactionManager());

            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    getDaoFactory().getBookDao().update(book);
                    getDaoFactory().getBorrowingDao().update(borrowing);
                }
            });
        }catch (Exception exception){
            exception.getStackTrace();
        }
    }

    /**
     * <b>Crée un emprunt pour un ouvrage</b>
     *
     * <p>Permet de créer un emprunt lié à un client et un ouvrage</p>
     *
     * @param clientId l'id du client faisant l'emprunt
     *
     * @param bookId l'id de l'ouvrage emprunté
     *
     * @throws ClientNotFoundException
     * @throws BookNotFoundException
     * @throws
     */
    public void borrow(Integer clientId, Integer bookId) throws ClientNotFoundException, BookNotFoundException, BookUnavailableException{
        BorrowingInterface borrowing = new Borrowing();
        ClientInterface client;
        BookInterface book;

        borrowing.setBorrowingDate(new Date());
        borrowing.setDeadLineForRendering(new Date(borrowing.getDeadLineForRendering().getTime() + TimeUnit.DAYS.toMillis(7 * this.initialPeriod)));

        try{
            client = this.getDaoFactory().getClientDao().findOneById(clientId);
            book = this.getDaoFactory().getBookDao().findOneById(bookId);

            if( client == null ){
                throw new ClientNotFoundException("Le client d'id " + clientId + " n'a pas été trouvé");
            }
            if( book == null ){
                throw new BookNotFoundException("L'ouvrage d'id " + bookId + " n'a pas été trouvé");
            }

            if(book.getNumberAvailable() < 1){
                log.info("L'ouvrage \"" + book.getTitle() + "\" est en rupture de stock.");
                throw new BookUnavailableException("Cette ouvrage n'est pas disponible");
            }

            borrowing.setClient(clientId);
            borrowing.setBook(bookId);

            book.setNumberAvailable(book.getNumberAvailable()-1);
            this.getDaoFactory().getBookDao().update(book);

            this.getDaoFactory().getBorrowingDao().insert(borrowing);


        }catch (Exception exception){
            exception.getMessage();
            log.error(exception.getMessage());
        }

    }
}
