package fr.librarymanager.business.manager;

import fr.librarymanager.consumer.dao.DaoFactoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

public class AbstractManager {

    @Autowired
    private DaoFactoryInterface daoFactory;

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    protected DaoFactoryInterface getDaoFactory(){
        return this.daoFactory;
    }

    protected PlatformTransactionManager getPlatformTransactionManager(){
        return this.platformTransactionManager;
    }
}
