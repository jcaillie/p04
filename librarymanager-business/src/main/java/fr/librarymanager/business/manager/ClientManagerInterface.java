package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.ClientNotFoundException;
import fr.librarymanager.model.ClientInterface;

/**
 * <b>Manager du bean Client</b>
 *
 * <p>Manager du bean Client permettant de gérer le code métier lié aux Clients</p>
 */
public interface ClientManagerInterface {
    ClientInterface getOneByEmail(String email) throws ClientNotFoundException;
    ClientInterface getOneById(Integer id) throws ClientNotFoundException;
}
