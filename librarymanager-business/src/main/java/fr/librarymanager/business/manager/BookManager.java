package fr.librarymanager.business.manager;

import fr.librarymanager.business.exception.BookNotFoundException;
import fr.librarymanager.model.BookInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <b>Manager du Bean Book</b>
 *
 * <p>Manager du Bean Book permettant de gérer le code métier du Bean Book</p>
 *
 */
@Component
public class BookManager extends AbstractManager implements BookManagerInterface {
    /**
     * <b>Récupère un ouvrage par son id</b>
     *
     * <p>Permet de récupérer un ouvrage en fonction de son id passé en paramètre.</p>
     *
     * @param id l'id de l'ouvrage recherché
     *
     * @return un bean Book représentant l'ouvrage recherché.
     */
    public BookInterface getOneById(Integer id) throws BookNotFoundException {
        BookInterface book = this.getDaoFactory().getBookDao().findOneById(id);

        if(book == null){
            throw new BookNotFoundException("L'ouvrage demandé d'id "+ id +" n'a pas été trouvé");
        }

        return book;
    }

    /**
     * <b>Recherche multicritère d'ouvrage</b>
     *
     * <p>Permet d'effectuer une recherche multicritère sur les ouvrages de l'application.</p>
     *
     * @param title le titre d'un ouvrage
     * @param author l'auteur d'un ou plusieurs ouvrages
     * @param isbn l'isbn d'un ouvrage
     *
     * @return La liste des ouvrages trouvés
     * @throws BookNotFoundException
     */
    public List<BookInterface> search(String title, String author, String isbn) throws BookNotFoundException {
        List<BookInterface> booksList = this.getDaoFactory().getBookDao().search("%"+title+"%", "%"+author+"%", "%"+isbn+"%");

        if(booksList == null){
            throw new BookNotFoundException("Aucun ouvrage trouvé selon vos critères.");
        }

        return booksList;
    }


}
