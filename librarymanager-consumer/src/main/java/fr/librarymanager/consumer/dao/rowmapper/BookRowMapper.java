package fr.librarymanager.consumer.dao.rowmapper;

import fr.librarymanager.model.Book;
import fr.librarymanager.model.BookInterface;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookRowMapper implements RowMapper<BookInterface> {
    @Override
    public BookInterface mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        BookInterface book = new Book();

        book.setId(resultSet.getInt("id"));
        book.setTitle(resultSet.getString("title"));
        book.setDescription(resultSet.getString("description"));
        book.setAuthor(resultSet.getString("author"));
        book.setIsbn(resultSet.getString("isbn"));
        book.setNumberAvailable(resultSet.getInt("number_available"));
        book.setNumberOfCopies(resultSet.getInt("number_of_copies"));
        book.setReleaseDate(resultSet.getDate("release_date"));

        return book;
    }
}
