package fr.librarymanager.consumer.dao;

import fr.librarymanager.consumer.dao.rowmapper.ClientRowMapper;
import fr.librarymanager.model.ClientInterface;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <b>Gestion Dao du bean Client</b>
 *
 * <p>Gère la communication avec la base de données pour l'utilisation du Bean Client</p>
 *
 * @author Jeremy Caillié
 */
@Component
public class ClientDao extends AbstractDao implements ClientDaoInterface{

    /**
     * <b>Cherche un client par son email</b>
     *
     * <p>Recherche dans la base de données, un client en fonction de son email.</p>
     *
     * @param email l'email du client recherché
     *
     * @return Une instance du bean Client représentant le client ou null si non trouvé.
     */
    public ClientInterface findOneByEmail(String email) {
        String sqlQuery = "SELECT * FROM library_manager.client WHERE email = ?";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<ClientInterface> client = jdbcTemplate.query(sqlQuery, new ClientRowMapper(), email);

        if(client.size() == 0){
            return null;
        }
        else{
            return client.get(0);
        }
    }

    /**
     * <b>Cherche un client par son id</b>
     *
     * <p>Recherche dans la base de données, un client en fonction de son id.</p>
     *
     * @param id l'id du client
     *
     * @return le client trouvé ou null
     */
    public ClientInterface findOneById(Integer id){
        String sqlQuery = "SELECT * FROM library_manager.client WHERE id = ?";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<ClientInterface> client = jdbcTemplate.query(sqlQuery, new ClientRowMapper(), id);

        if(client.size() == 0){
            return null;
        }else{
            return client.get(0);
        }
    }
}
