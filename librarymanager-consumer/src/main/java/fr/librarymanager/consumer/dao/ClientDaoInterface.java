package fr.librarymanager.consumer.dao;

import fr.librarymanager.model.ClientInterface;

/**
 * <b>Interface ClientDaoInterface</b>
 *
 * <p>Permet de gérer l'abstraction de la DAO ClientDao</p>
 *
 * @author  Jeremy Caillié
 */
public interface ClientDaoInterface {
    ClientInterface findOneByEmail(String email);
    ClientInterface findOneById(Integer id);
}
