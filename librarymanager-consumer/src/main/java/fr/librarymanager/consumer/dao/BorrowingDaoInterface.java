package fr.librarymanager.consumer.dao;

import fr.librarymanager.model.BorrowingInterface;

import java.util.List;

/**
 * <b>Interface de la DAO BorrowingDao</b>
 *
 * <p>Interface permettant de gérer l'abstraction de la DAO BorrowingDao.</p>
 *
 * @author Jeremy Caillié
 */
public interface BorrowingDaoInterface {
    List<BorrowingInterface> findAllByClientId(Integer clientId);
    List<BorrowingInterface> findAllLate();
    BorrowingInterface findOneById(Integer id);
    void update(BorrowingInterface borrowing);
    void insert(BorrowingInterface newBorrowing);
}
