package fr.librarymanager.consumer.dao;

import fr.librarymanager.consumer.dao.rowmapper.BookRowMapper;
import fr.librarymanager.model.BookInterface;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <b>Gestion Dao du bean Book</b>
 *
 * <p>Gère la communication avec la base de données pour l'utilisation du Bean Book</p>
 *
 * @author Jeremy Caillié
 */
@Component
public class BookDao extends AbstractDao implements BookDaoInterface {

    /**
     * <b>Cherche un ouvrage par son id</b>
     *
     * <p>Recherche dans la base de données, un ouvrage en fonction de son id.</p>
     *
     * @param id l'id de l'ouvrage recherché
     *
     * @return Une instance du bean Book représentant l'ouvrage ou null si non trouvé.
     */
    public BookInterface findOneById(Integer id) {
        String sqlQuery = "SELECT * FROM library_manager.book WHERE id = ?";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<BookInterface> book = jdbcTemplate.query(sqlQuery, new BookRowMapper(), id);

        if(book.size() == 0){
            return null;
        }
        else{
            return book.get(0);
        }
    }

    /**
     * <b>Fait une recherche d'ouvrage par critère</b>
     *
     * <p>Permet d'éffectuer une recherche d'ouvrage multi critères dans la base de données en fonction des critères passés en paramètre.</p>
     *
     * @param title titre d'un ouvrage
     * @param author auteur d'un ou plusieurs ouvrage
     * @param isbn isbn d'un ouvrage
     *
     * @return La liste des ouvrages trouvés ou null si non trouvé.
     */
    public List<BookInterface> search(String title, String author, String isbn) {

        String sqlQuery = "SELECT * FROM library_manager.book WHERE LOWER(title) LIKE LOWER(?) AND LOWER(author) LIKE LOWER(?) AND LOWER(isbn) LIKE LOWER(?)";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<BookInterface> bookList = jdbcTemplate.query(sqlQuery, new BookRowMapper(), title, author, isbn);

        if(bookList.size() == 0){
            return null;
        }
        else{
            return bookList;
        }
    }

    /**
     * <b>Met à jour un enregistrement d'ourage</b>
     *
     * <p>Permet de mettre à jour un enregistrement d'ouvrage dans la base de données.</p>
     *
     * @param book le bean Book à persister
     */
    public void update(BookInterface book){
        String sqlQuery = "UPDATE library_manager.book SET title=:title, description=:description, author=:author, release_date=:releaseDate, isbn=:isbn, number_of_copies=:numberOfCopies, number_available=:numberAvailable";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());

        MapSqlParameterSource params = this.createParams(book);

        jdbcTemplate.update(sqlQuery, params);
    }


    /**
     * <b>crée un mapSqlParameterSource mappé sur un bean Book.</b>
     *
     * <p>Permet de créer un mapSqlParameterSource mappé avec les données d'un bean Book fourni en parametre, pouvant service à créer ou mettre à jour un bean Book.</p>
     *
     * @param book le bean Book servant à mapper le mapSqlParameterSource
     *
     * @return un MapSqlParameterSource mappé sur le bean Book
     */
    private MapSqlParameterSource createParams(BookInterface book){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("id", book.getId());
        params.addValue("title", book.getTitle());
        params.addValue("description", book.getDescription());
        params.addValue("author", book.getAuthor());
        params.addValue("releaseDate", book.getReleaseDate());
        params.addValue("isbn", book.getIsbn());
        params.addValue("numberOfCopies", book.getNumberOfCopies());
        params.addValue("numberAvailable", book.getNumberAvailable());

        return params;
    }

}
