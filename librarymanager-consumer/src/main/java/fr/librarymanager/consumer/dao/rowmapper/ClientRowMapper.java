package fr.librarymanager.consumer.dao.rowmapper;

import fr.librarymanager.model.Client;
import fr.librarymanager.model.ClientInterface;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientRowMapper implements RowMapper<ClientInterface> {
    @Override
    public ClientInterface mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        ClientInterface client = new Client();

        client.setId(resultSet.getInt("id"));
        client.setFirstname(resultSet.getString("firstname"));
        client.setLastname(resultSet.getString("lastname"));
        client.setBirthday(resultSet.getDate("birthday"));
        client.setEmail(resultSet.getString("email"));
        client.setEncryptedPassword(resultSet.getString("encrypted_password"));
        client.setToken(resultSet.getString("token"));

        return client;
    }
}
