package fr.librarymanager.consumer.dao;

import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;

public class AbstractDao {

    @Autowired
    private DataSource dataSource;

    protected DataSource getDataSource(){
        return this.dataSource;
    }
}
