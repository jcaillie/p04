package fr.librarymanager.consumer.dao;

import fr.librarymanager.consumer.dao.rowmapper.BorrowingRowMapper;
import fr.librarymanager.model.BorrowingInterface;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * <b>Dao du Bean Borrowing</b>
 *
 * <p>Dao permettant de gérer la communication la base de données afin de manipuler les données des Beans Borrowing</p>
 */
@Component
public class BorrowingDao extends AbstractDao implements BorrowingDaoInterface {

    /**
     * <b>Récupère la liste des emprunts d'un client</b>
     *
     * <p>Récupère la liste complete des emprunts réalisés par un client, trié par ordre décroissant de date de réservation.</p>
     *
     * @param clientId l'id du client dont les emprunts sont recherchés.
     *
     * @return la liste des emprunts du client existant null
     */
    public List<BorrowingInterface> findAllByClientId(Integer clientId) {

        String sqlQuery = "SELECT * FROM library_manager.borrowing WHERE client_id = ? ORDER BY borrowing_date DESC";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<BorrowingInterface> borrowingList = jdbcTemplate.query(sqlQuery, new BorrowingRowMapper(), clientId);

        if(borrowingList.size() == 0){
            return null;
        }
        else{
            return borrowingList;
        }
    }

    /**
     * <b>Récupère la liste des emprunts dont la date de rendu est dépassé</b>
     *
     * <p>Permet de récupérer dans la base de données la liste de tous les emprunts dont la date de rendu est dépassé et qui n'ont pas été rendu.</p>
     *
     * @return la liste des emprunts ou null si aucun emprunt correspondant aux critères n'a été trouvé.
     */
    public List<BorrowingInterface> findAllLate(){

        String sqlQuery = "SELECT * FROM library_manager.borrowing WHERE rendered_up IS NULL AND deadline_for_rendering < CURRENT_DATE ORDER BY client_id, deadline_for_rendering ASC";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<BorrowingInterface> borrowingList = jdbcTemplate.query(sqlQuery, new BorrowingRowMapper());

        if(borrowingList.size() == 0){
            return null;
        }
        else{
            return borrowingList;
        }
    }


    /**
     *  <b>Met à jour un emprunt</b>
     *
     *  <p>Permet de mettre à jour un enregistrement d'un emprunt dans la base de données.</p>
     *
     * @param borrowing l'emprunt à mettre à jour
     */
    public void update(BorrowingInterface borrowing) {
        String sqlQuery = "UPDATE library_manager.borrowing SET extended = true, deadline_for_rendering = :deadLine WHERE number = :number";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("deadLine", borrowing.getDeadLineForRendering());
        params.addValue("number", borrowing.getNumber());

        jdbcTemplate.update(sqlQuery, params);
    }


    /**
     * <b>Récupère un emprunt par son id</b>
     *
     * <p> Permet de récupérer en base de données, un emprunt en fonction de son id passé en paramètre. Si aucun emprunt n'est trouvé, renvoie null.</p>
     *
     * @param id de l'emprunt recherché
     *
     * @return l'emprunt recherché ou null
     */
    public BorrowingInterface findOneById(Integer id) {
        String sqlQuery = "SELECT * FROM library_manager.borrowing WHERE number = ?";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

        List<BorrowingInterface> borrowingList = jdbcTemplate.query(sqlQuery, new BorrowingRowMapper(), id);

        if(borrowingList.size() == 0){
            return null;
        }
        else{
            return borrowingList.get(0);
        }
    }

    /**
     * <b>Enregistre un nouvel emprunt.</b>
     *
     * <p>Enregistre un nouvel emprunt dans la base de données.</p>
     *
     * @param newBorrowing le nouvel emprunt
     */
    public void insert(BorrowingInterface newBorrowing){
        String sqlQuery = "INSERT INTO library_manager.borrowing (borrowing_date, deadline_for_rendering, extended, rendered_up, client_id, book_id) VALUES (:borrowingDate, :deadlineForRendering, :extended, :renderedUp, :clientId, :bookId))";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());

        MapSqlParameterSource params = this.createParams(newBorrowing);

        jdbcTemplate.update(sqlQuery, params);
    }


    /**
     * <b>crée un mapSqlParameterSource mappé sur un bean Borrowing.</b>
     *
     * <p>Permet de créer un mapSqlParameterSource mappé avec les données d'un bean Borrowing fourni en parametre, pouvant service à créer ou mettre à jour un bean Borrowing.</p>
     *
     * @param borrowing le bean Borrowing servant à mapper le mapSqlParameterSource
     *
     * @return un MapSqlParameterSource mappé sur le bean Borrowing
     */
    private MapSqlParameterSource createParams(BorrowingInterface borrowing){
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("number", borrowing.getNumber());
        params.addValue("borrowingDate", borrowing.getBorrowingDate());
        params.addValue("deadlineForRendering", borrowing.getDeadLineForRendering());
        params.addValue("extended", borrowing.getExtended());
        params.addValue("renderedUp", borrowing.getRenderedUp());
        params.addValue("clientId", borrowing.getClient());
        params.addValue("bookId", borrowing.getBook());

        return params;
    }
}
