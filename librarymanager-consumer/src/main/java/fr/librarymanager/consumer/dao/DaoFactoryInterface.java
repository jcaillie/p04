package fr.librarymanager.consumer.dao;

public interface DaoFactoryInterface {
    ClientDaoInterface getClientDao();
    BookDaoInterface getBookDao();
    BorrowingDaoInterface getBorrowingDao();
}
