package fr.librarymanager.consumer.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DaoFactory implements DaoFactoryInterface{

    @Autowired
    private ClientDaoInterface clientDao;

    @Autowired
    private BookDaoInterface bookDao;

    @Autowired
    private BorrowingDaoInterface borrowingDao;

    public ClientDaoInterface getClientDao() {
        return clientDao;
    }

    public BookDaoInterface getBookDao() {
        return bookDao;
    }

    public BorrowingDaoInterface getBorrowingDao() { return borrowingDao; }
}
