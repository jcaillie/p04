package fr.librarymanager.consumer.dao.rowmapper;

import fr.librarymanager.model.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BorrowingRowMapper implements RowMapper<BorrowingInterface> {
    @Override
    public BorrowingInterface mapRow(ResultSet resultSet, int rowNumber) throws SQLException {

        BorrowingInterface borrowing = new Borrowing();

        borrowing.setNumber(resultSet.getInt("number"));
        borrowing.setBorrowingDate(resultSet.getDate("borrowing_date"));
        borrowing.setExtended(resultSet.getBoolean("extended"));
        borrowing.setRenderedUp(resultSet.getDate("rendered_up"));
        borrowing.setDeadLineForRendering(resultSet.getDate("deadline_for_rendering"));
        borrowing.setClient(resultSet.getInt("client_id"));
        borrowing.setBook(resultSet.getInt("book_id"));

        return borrowing;
    }
}
