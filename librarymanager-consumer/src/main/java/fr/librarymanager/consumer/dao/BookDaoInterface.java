package fr.librarymanager.consumer.dao;

import fr.librarymanager.model.BookInterface;

import java.util.List;

/**
 * <b>Interface BookDaoInterface</b>
 *
 * <p>Permet de gérer l'abstraction de la DAO BookDao</p>
 *
 * @author  Jeremy Caillié
 */
public interface BookDaoInterface {
    BookInterface findOneById(Integer id );
    List<BookInterface> search(String title, String author, String isbn);
    void update(BookInterface book);
}
