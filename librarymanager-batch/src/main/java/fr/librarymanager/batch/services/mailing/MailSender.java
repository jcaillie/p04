package fr.librarymanager.batch.services.mailing;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


/**
 * <b>service de gestion d'envoie d'email</b>
 *
 * <p>Service permettant d'envoyer des mails avec les propriétés d'application.properties injectées par Spring.</p>
 */
@Component
@PropertySource("classpath:application.properties")
public class MailSender {

    @Value("${service.email.host}")
    private String host;
    @Value("${service.email.port}")
    private int port;
    @Value("${service.email.from}")
    private String from;
    @Value("${service.email.username}")
    private String username;
    @Value("${service.email.password}")
    private String password;
    @Value("${service.email.subject}")
    private String subject;

    public void send(String to, String htmlContent) throws MessagingException {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        Properties properties = System.getProperties();

        properties.setProperty("mail.smtp.host", host);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.user", this.username);
        properties.setProperty("mail.smtp.password", this.password);

        Session session = Session.getDefaultInstance(properties);
        session.setDebug(true);

        mailSender.setHost(this.host);
        mailSender.setJavaMailProperties(properties);
        mailSender.setUsername(this.username);
        mailSender.setPassword(this.password);
        mailSender.setProtocol("smtp");
        mailSender.setPort(this.port);


        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setTo(to);

        helper.setText(htmlContent, true);

        mailSender.send(message);
    }
}
