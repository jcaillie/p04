
package fr.librarymanager.batch.services.borrowingService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.librarymanager.batch.services.borrowingService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllBorrowingByClientId_QNAME = new QName("http://webservice.librarymanager.fr/", "getAllBorrowingByClientId");
    private final static QName _GetAllBorrowingByClientIdResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "getAllBorrowingByClientIdResponse");
    private final static QName _GetAllBorrowingLate_QNAME = new QName("http://webservice.librarymanager.fr/", "getAllBorrowingLate");
    private final static QName _BorrowingServiceException_QNAME = new QName("http://webservice.librarymanager.fr/", "BorrowingServiceException");
    private final static QName _GetAllBorrowingLateResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "getAllBorrowingLateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.librarymanager.batch.services.borrowingService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllBorrowingByClientIdResponse }
     * 
     */
    public GetAllBorrowingByClientIdResponse createGetAllBorrowingByClientIdResponse() {
        return new GetAllBorrowingByClientIdResponse();
    }

    /**
     * Create an instance of {@link GetAllBorrowingByClientId }
     * 
     */
    public GetAllBorrowingByClientId createGetAllBorrowingByClientId() {
        return new GetAllBorrowingByClientId();
    }

    /**
     * Create an instance of {@link GetAllBorrowingLateResponse }
     * 
     */
    public GetAllBorrowingLateResponse createGetAllBorrowingLateResponse() {
        return new GetAllBorrowingLateResponse();
    }

    /**
     * Create an instance of {@link GetAllBorrowingLate }
     * 
     */
    public GetAllBorrowingLate createGetAllBorrowingLate() {
        return new GetAllBorrowingLate();
    }

    /**
     * Create an instance of {@link BorrowingServiceFault }
     * 
     */
    public BorrowingServiceFault createBorrowingServiceFault() {
        return new BorrowingServiceFault();
    }

    /**
     * Create an instance of {@link Borrowing }
     * 
     */
    public Borrowing createBorrowing() {
        return new Borrowing();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBorrowingByClientId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getAllBorrowingByClientId")
    public JAXBElement<GetAllBorrowingByClientId> createGetAllBorrowingByClientId(GetAllBorrowingByClientId value) {
        return new JAXBElement<GetAllBorrowingByClientId>(_GetAllBorrowingByClientId_QNAME, GetAllBorrowingByClientId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBorrowingByClientIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getAllBorrowingByClientIdResponse")
    public JAXBElement<GetAllBorrowingByClientIdResponse> createGetAllBorrowingByClientIdResponse(GetAllBorrowingByClientIdResponse value) {
        return new JAXBElement<GetAllBorrowingByClientIdResponse>(_GetAllBorrowingByClientIdResponse_QNAME, GetAllBorrowingByClientIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBorrowingLate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getAllBorrowingLate")
    public JAXBElement<GetAllBorrowingLate> createGetAllBorrowingLate(GetAllBorrowingLate value) {
        return new JAXBElement<GetAllBorrowingLate>(_GetAllBorrowingLate_QNAME, GetAllBorrowingLate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrowingServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "BorrowingServiceException")
    public JAXBElement<BorrowingServiceFault> createBorrowingServiceException(BorrowingServiceFault value) {
        return new JAXBElement<BorrowingServiceFault>(_BorrowingServiceException_QNAME, BorrowingServiceFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBorrowingLateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getAllBorrowingLateResponse")
    public JAXBElement<GetAllBorrowingLateResponse> createGetAllBorrowingLateResponse(GetAllBorrowingLateResponse value) {
        return new JAXBElement<GetAllBorrowingLateResponse>(_GetAllBorrowingLateResponse_QNAME, GetAllBorrowingLateResponse.class, null, value);
    }

}
