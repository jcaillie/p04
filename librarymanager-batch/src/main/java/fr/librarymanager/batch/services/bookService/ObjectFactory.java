
package fr.librarymanager.batch.services.bookService;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.librarymanager.batch.services.bookService package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SearchBooksResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "searchBooksResponse");
    private final static QName _GetBookByIdResponse_QNAME = new QName("http://webservice.librarymanager.fr/", "getBookByIdResponse");
    private final static QName _SearchBooks_QNAME = new QName("http://webservice.librarymanager.fr/", "searchBooks");
    private final static QName _GetBookById_QNAME = new QName("http://webservice.librarymanager.fr/", "getBookById");
    private final static QName _BookServiceException_QNAME = new QName("http://webservice.librarymanager.fr/", "BookServiceException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.librarymanager.batch.services.bookService
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBookByIdResponse }
     * 
     */
    public GetBookByIdResponse createGetBookByIdResponse() {
        return new GetBookByIdResponse();
    }

    /**
     * Create an instance of {@link SearchBooks }
     * 
     */
    public SearchBooks createSearchBooks() {
        return new SearchBooks();
    }

    /**
     * Create an instance of {@link SearchBooksResponse }
     * 
     */
    public SearchBooksResponse createSearchBooksResponse() {
        return new SearchBooksResponse();
    }

    /**
     * Create an instance of {@link BookServiceFault }
     * 
     */
    public BookServiceFault createBookServiceFault() {
        return new BookServiceFault();
    }

    /**
     * Create an instance of {@link GetBookById }
     * 
     */
    public GetBookById createGetBookById() {
        return new GetBookById();
    }

    /**
     * Create an instance of {@link Book }
     * 
     */
    public Book createBook() {
        return new Book();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchBooksResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "searchBooksResponse")
    public JAXBElement<SearchBooksResponse> createSearchBooksResponse(SearchBooksResponse value) {
        return new JAXBElement<SearchBooksResponse>(_SearchBooksResponse_QNAME, SearchBooksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBookByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getBookByIdResponse")
    public JAXBElement<GetBookByIdResponse> createGetBookByIdResponse(GetBookByIdResponse value) {
        return new JAXBElement<GetBookByIdResponse>(_GetBookByIdResponse_QNAME, GetBookByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchBooks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "searchBooks")
    public JAXBElement<SearchBooks> createSearchBooks(SearchBooks value) {
        return new JAXBElement<SearchBooks>(_SearchBooks_QNAME, SearchBooks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBookById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "getBookById")
    public JAXBElement<GetBookById> createGetBookById(GetBookById value) {
        return new JAXBElement<GetBookById>(_GetBookById_QNAME, GetBookById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BookServiceFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.librarymanager.fr/", name = "BookServiceException")
    public JAXBElement<BookServiceFault> createBookServiceException(BookServiceFault value) {
        return new JAXBElement<BookServiceFault>(_BookServiceException_QNAME, BookServiceFault.class, null, value);
    }

}
