package fr.librarymanager.batch.services;


import fr.librarymanager.batch.services.bookService.BookService;
import fr.librarymanager.batch.services.borrowingService.BorrowingService;
import fr.librarymanager.batch.services.clientService.ClientService;

public interface ServicesFactoryInterface {
    public BookService getBookService();
    public BorrowingService getBorrowingService();
    public ClientService getClientService();
}
