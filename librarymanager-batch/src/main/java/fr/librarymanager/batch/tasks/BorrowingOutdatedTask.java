package fr.librarymanager.batch.tasks;

import fr.librarymanager.batch.listeners.SendMailListener;
import fr.librarymanager.batch.services.ServicesFactory;
import fr.librarymanager.batch.services.bookService.BookService;
import fr.librarymanager.batch.services.bookService.Book;
import fr.librarymanager.batch.services.bookService.BookServiceException;
import fr.librarymanager.batch.services.bookService.BookServiceService;
import fr.librarymanager.batch.services.borrowingService.*;
import fr.librarymanager.batch.services.clientService.Client;
import fr.librarymanager.batch.services.clientService.ClientService;
import fr.librarymanager.batch.services.clientService.ClientServiceException;
import fr.librarymanager.batch.services.clientService.ClientServiceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>Tache de gestion des emprunts en retards</b>
 *
 * <p>Permet de gérer l'envoie des mails d'information aux clients dont des emprunts sont en retard.</p>
 *
 * @author Jeremy Caillié
 */
@PropertySource("classpath:application.properties")
@Component
public class BorrowingOutdatedTask {

    private static final Logger log = LogManager.getLogger(BorrowingOutdatedTask.class);

    @Autowired
    private SendMailListener sendMailListener;

    @Autowired
    private ServicesFactory servicesFactory;

    /**
     * <b>Step de récupération et de traitement des emprunts en retard.</b>
     *
     * <p>S'occupe de récupérer la liste complête des emprunts en retard, puis la traite de façon à générer un mail récapitulatif pour chaque client ayant des emprunts en retards.</p>
     */
    @Scheduled(cron = "${service.email.cron}")
    public void sendMailForBorrowingOutdated(){

        log.info("Lancement de la tache d'envoie d'email de relance pour les emprunts en retard.");

        try {

            Client client = new Client();
            Book book;
            List<Borrowing> borrowingsOfClient = new ArrayList<>();

            List<Borrowing> borrowingOutdated = this.servicesFactory.getBorrowingService().getAllBorrowingLate();
            Map<Integer, Book> booksOfBorrowing = new HashMap<>();

            for (Borrowing borrowing: borrowingOutdated) {
                // On récupère les informations de l'ouvrage
                book = this.servicesFactory.getBookService().getBookById(borrowing.getBook());

                // Si on est sur la liste d'un nouveau client, alors on envoie un mail au client précédent récapitulant les emprunts, puis on crée une nouvelle liste.
                if((borrowing.getClient() != client.getId()) || (client.getId() == null)){

                    if(borrowingsOfClient.size() != 0){
                        this.sendMailListener.createAndSend(client, borrowingsOfClient, booksOfBorrowing);
                    }
                    client = this.servicesFactory.getClientService().getClientById(borrowing.getClient());

                    borrowingsOfClient.clear();
                    booksOfBorrowing.clear();
                    borrowingsOfClient.add(borrowing);
                    booksOfBorrowing.put(borrowing.getNumber(), book);
                }else{
                    // Sinon on ajoute les informations client au borrowing et on l'ajoute dans la liste des emprunts du client en cours.

                    borrowingsOfClient.add(borrowing);
                    booksOfBorrowing.put(borrowing.getNumber(), book);
                }

                // On envoie le mail d'information du dernier client listé.
                if(borrowingsOfClient.size()>0){
                    this.sendMailListener.createAndSend(client, borrowingsOfClient, booksOfBorrowing);
                }
            }
        } catch (BorrowingServiceException e) {
            log.info(e.getMessage());
        } catch (ClientServiceException e) {
            log.info(e.getMessage());
        } catch (MessagingException e) {
            log.info(e.getMessage());
        } catch (BookServiceException e) {
            log.info(e.getMessage());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        log.info("Fin de l'envoie des emails de relance.");
    }
}
