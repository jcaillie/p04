package fr.librarymanager.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "fr.librarymanager.batch")
@EnableScheduling
public class LibrarymanagerBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibrarymanagerBatchApplication.class, args);
	}
}
