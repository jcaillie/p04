package fr.librarymanager.batch.listeners;

import fr.librarymanager.batch.services.bookService.Book;
import fr.librarymanager.batch.services.borrowingService.Borrowing;
import fr.librarymanager.batch.services.clientService.Client;
import fr.librarymanager.batch.services.mailing.MailSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * <b>listener d'envoie de mail</b>
 *
 * <p>Listener s'occupant de créer le contenu du mail avant d'en demander l'envoie.</p>
 */
@Component
public class SendMailListener {

    private static final Logger log = LogManager.getLogger(SendMailListener.class);

    @Autowired
    private MailSender mailSender;

    public void createAndSend(Client client, List<Borrowing> borrowings, Map<Integer, Book> booksOfBorrowing) throws MessagingException {
        String content = this.createMessage(client, borrowings, booksOfBorrowing);

        log.info("Envoie d'un email à l'adresse " + client.getEmail() + " pour " + borrowings.size() + " emprunts en retard.");

        this.mailSender.send(client.getEmail(), content);
    }

    private String createMessage(Client client, List<Borrowing> borrowings, Map<Integer, Book> booksOfBorrowing){
        String message;

        message = "<html><head><meta charset=\"utf-8\"/><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\"></head>";
        message += "<body><div class=\"card\"><div class=\"card-header bg-info text-center\"><h3>LibraryManager</h3></div>";
        message += "<div class=\"card-body\"><h5 class=\"card-title\">Bonjour " + client.getFirstname() + " " + client.getLastname() + "</h5>";
        message += "<p class=\"card-text\">Voici la liste de vos emprunts en retard dans notre bibliothèque:</p>";
        message += "<ul class=\"list-group\">";

        for (Borrowing borrowing: borrowings) {
            message += "<li class=\"list-group-item d-flex justify-content-between align-items-center\">";
            message += "\"" + booksOfBorrowing.get(borrowing.getNumber()).getTitle() + "\" de " + booksOfBorrowing.get(borrowing.getNumber()).getAuthor();
            message += "<span class=\"badge badge-danger badge-pill\">Date limite: " + borrowing.getDeadLineForRendering().getDay()+ "/"+borrowing.getDeadLineForRendering().getMonth()+"/"+borrowing.getDeadLineForRendering().getYear() + "</span>";
            message += "</li>";
        }

        message += "</ul></div></div></body></html>";

        return message;
    }
}
