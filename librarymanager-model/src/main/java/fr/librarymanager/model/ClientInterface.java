package fr.librarymanager.model;

import java.util.Date;

/**
 * <b>Interface Client</b>
 *
 * <p>
 *     Interface permettant l'abstraction de la classe Client représentant un utilisateur de la bibliothèque.
 * </p>
 *
 * @author Jeremy Caillié
 */
public interface ClientInterface {
    Integer getId();
    void setId(Integer id);
    String getFirstname();
    void setFirstname(String firstname);
    String getLastname();
    void setLastname(String lastname);
    Date getBirthday();
    void setBirthday(Date birthday);
    String getEmail();
    void setEmail(String email);
    String getEncryptedPassword();
    void setEncryptedPassword(String encryptedPassword);
    String getToken();
    void setToken(String token);
}
