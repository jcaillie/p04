package fr.librarymanager.model;

import java.util.Date;

/**
 * <b>Bean Book</b>
 *
 * <p>Représentation fonctionnel d'un ouvrage de la bibliothèque.</p>
 *
 * @author Jeremy Caillié
 */
public class Book implements BookInterface {

    private Integer id;
    private String title;
    private String description;
    private String author;
    private Date releaseDate;
    private String isbn;
    private Integer numberOfCopies;
    private Integer numberAvailable;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public Date getReleaseDate() {
        return releaseDate;
    }

    @Override
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public Integer getNumberOfCopies() {
        return numberOfCopies;
    }

    @Override
    public void setNumberOfCopies(Integer numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    @Override
    public Integer getNumberAvailable() {
        return numberAvailable;
    }

    @Override
    public void setNumberAvailable(Integer numberAvailable) {
        this.numberAvailable = numberAvailable;
    }
}
