package fr.librarymanager.model;

import java.util.Date;

/**
 * <b>Bean Borrowing</b>
 *
 * <p>Bean Borrowing représentant un emprunt d'ouvrage dans la bibliothèque.</p>
 *
 * @author Jeremy Caillié
 */
public class Borrowing implements BorrowingInterface {

    private Integer number;
    private Date BorrowingDate;
    private Date deadLineForRendering;
    private Boolean extended;
    private Date renderedUp = null;
    private Integer clientId;
    private Integer bookId;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getBorrowingDate() {
        return BorrowingDate;
    }

    public void setBorrowingDate(Date borrowingDate) {
        BorrowingDate = borrowingDate;
    }

    public Date getDeadLineForRendering() {
        return deadLineForRendering;
    }

    public void setDeadLineForRendering(Date deadLineForRendering) {
        this.deadLineForRendering = deadLineForRendering;
    }

    public Boolean getExtended() {
        return extended;
    }

    public void setExtended(Boolean extended) {
        this.extended = extended;
    }

    public Date getRenderedUp() {
        return renderedUp;
    }

    public void setRenderedUp(Date renderedUp) {
        this.renderedUp = renderedUp;
    }

    public Integer getClient() {
        return clientId;
    }

    public void setClient(Integer client) {
        this.clientId = client;
    }

    public Integer getBook() {
        return bookId;
    }

    public void setBook(Integer book) {
        this.bookId = book;
    }
}
