package fr.librarymanager.model;

import java.util.Date;

/**
 * <b>Interface du Bean Borrowing</b>
 *
 * <p>Permet de gérer l'abstraction du Bean Borrowing dans l'application.</p>
 *
 * @author Jeremy Caillié
 */
public interface BorrowingInterface {
    Integer getNumber();
    void setNumber( Integer number );
    Date getBorrowingDate();
    void setBorrowingDate( Date borrowingDate );
    Date getDeadLineForRendering();
    void setDeadLineForRendering( Date deadLineForRendering );
    Boolean getExtended();
    void setExtended( Boolean extended );
    Date getRenderedUp();
    void setRenderedUp( Date renderedUp );
    Integer getClient();
    void setClient( Integer clientId );
    Integer getBook();
    void setBook( Integer book );
}
