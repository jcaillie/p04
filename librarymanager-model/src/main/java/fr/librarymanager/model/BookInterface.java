package fr.librarymanager.model;

import java.util.Date;

public interface BookInterface {
    Integer getId();
    void setId(Integer id);
    String getTitle();
    void setTitle(String title);
    String getDescription();
    void setDescription(String description);
    String getAuthor();
    void setAuthor(String author);
    Date getReleaseDate();
    void setReleaseDate(Date releaseDate);
    String getIsbn();
    void setIsbn(String isbn);
    Integer getNumberOfCopies();
    void setNumberOfCopies(Integer numberOfCopies);
    Integer getNumberAvailable();
    void setNumberAvailable(Integer numberAvailable);
}
