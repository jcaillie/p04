package fr.librarymanager.model;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * <b>Bean Client</b>
 *
 * <p>Représentation fonctionnel d'un client de la bibliothèque.</p>
 *
 * @author Jeremy Caillié
 */
public class Client implements ClientInterface {

    private Integer id;

    @NotEmpty(message = "Le prénom est obligatoire")
    @Min(message = "Le prénom doit être composé d'au minimum 2 charactères.", value = 2)
    @Max(message = "Le prénom doit être composé d'au maximum 25 charactères.", value = 25)
    private String firstname;

    @NotEmpty(message = "Le nom est obligatoire")
    @Min(message = "Le nom doit être composé d'au minimum 2 charactères.", value = 2)
    @Max(message = "Le nom doit être composé d'au maximum 25 charactères.", value = 25)
    private String lastname;

    @NotNull(message = "La date de naissance est obligatoire.")
    private Date birthday;

    @NotNull(message = "L'email est obligatoire.")
    @Max(message = "La taille maximal de l'email est de 36 charactères.", value = 36)
    @Email(message = "Adresse email invalide.")
    private String email;

    @NotNull(message = "Un mot de passe est obligatoire")
    private String encryptedPassword;

    @NotNull(message = "Le token est obligatoire")
    private String token;


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncryptedPassword() {
        return this.encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
