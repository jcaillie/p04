# Déploiement du système

Le système est prévu pour pouvoir être déployé de deux façons différentes. En installation complète
sur un seul serveur, ou la possibilité de répartir les containers sur différents serveurs.

## 1) LA BASE DE DONNEES
Le fichier docker-compose.yml de la base de données se trouve dans le dossier bdd du dossier
docker. Le container est basé sur une image docker de Postgresql 9.

Plusieurs variables d'environnement peuvent être modifier afin de convenir à votre système tel que le nom et le mot de passe de connexion à la base de données, ainsi
que le port de redirection entre la machine hôte et le container.
Le dossier script contient tous les scripts de création de la base de données, du schéma et des
différentes tables, ainsi que l’insertion d’un jeu de données.

## 2) LES WEBSERVICES
Le fichier docker-compose.yml du webservice se trouve dans le dossier webservice du dossier
docker. Ce container se base sur une image de Tomcat en version 9.6 pour fonctionner. Se fichier
comporte différentes variables d’environnement permettant de configurer les webservices et que
vous pouvez modifier afin d’être en adéquation avec votre infrastructure. Vous pouvez également
configurer le port de redirection de la machine hôte vers le container.
Les variables d’environnement concernent l’url de la Datasource, le nom d’utilisateur à utiliser, le
mot de passe, la période initiale d’un emprunt, ainsi que la période d’extension d’un emprunt.
En plus de ces variables, vous avez la possibilité de modifier le fichier /configurations/tomcat-
users.xml afin de modifier le nom d’utilisateur et le mot de passe permettant d’accéder à Tomcat
depuis un navigateur.

## 3) LA WEBAPP
Le fichier docker-compose.yml de la Webapp se trouve dans le dossier webapp du dossier docker.
Ce container se base également sur une image de Tomcat en version 9.6 pour mettre à disposition
l’application au client. Le fichier contient les variables d’environnements à configurer afin de donner
les url d’accès aux différents webservices à l’application. Vous pouvez également modifier le port de
redirection de la machine hôte vers le container.
Comme pour les webservices, vous pouvez également modifier le fichier configurations/tomcat-
users.xml afin de configurer le nom d’utilisateur et mot de passe permettant d’accéder à Tomcat
depuis un navigateur.

## 4) LE BATCH
Le fichier docker-compose.yml du Batch se trouve dans le dossier docker/batch. Ce container se
base sur une image simple d’alpine-java permettant de limiter les ressources utilisées.
Ce container nécessite plus de configuration que les autres. En effet vous devez renseigner toutes
les informations liées au serveur d’envoie des emails, le cron, en plus des url d’accès aux différents
webservices. Ses variables sont, l’host du service mail, le port, l’expéditeur, le nom d’utilisateur de
l’expéditeur, le mot de passe, le sujet. Le Cron utilise la nomenclature * * * * * * que vous pouvez
modifier à votre convenance pour la redondance d’envoie des emails.

## 5) CONFIGURATION GENERALE
Il vous est possible également de préparer et lancer tous les containers en une seule fois si vous
prenez la décision de les faire tourner sur la même machine. Vous trouverez un fichier docker-
composer.yml à la racine du dossier docker, reprenant la configuration des différents containers du
système. Veuillez suivre les mêmes instructions que pour les différents containers afin de les
configurer.