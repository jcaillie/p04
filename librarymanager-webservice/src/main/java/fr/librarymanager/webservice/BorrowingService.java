package fr.librarymanager.webservice;

import fr.librarymanager.business.exception.*;
import fr.librarymanager.model.Borrowing;
import fr.librarymanager.model.BorrowingInterface;
import fr.librarymanager.webservice.exception.BorrowingServiceException;
import fr.librarymanager.webservice.exception.BorrowingServiceFault;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * <b>Web servie du bean Borrowing</b>
 *
 * <p>Web service permettant l'accès aux données des emprunts d'ouvrage.</p>
 */
@WebService
public class BorrowingService extends AbstractService {

    private static final Logger log = LogManager.getLogger(BorrowingService.class);

    /**
     * <b>Retourne la liste des emprunts d'un utilisateur</b>
     *
     * <p>Permet de récupérer la liste des emprunts d'un utilisateur ordonnées par date d'emprunt descendant.</p>
     *
     * @param clientId l'id du client auquel sont rattachés les emprunts.
     *
     * @return la liste des emprunts du client.
     *
     * @throws BorrowingServiceException
     */
    @WebMethod
    public List<Borrowing> getAllBorrowingByClientId(Integer clientId) throws BorrowingServiceException {
        List<BorrowingInterface> borrowingList;

        try{
            borrowingList = this.getManagerFactory().getBorrowingManager().getAllByClientId(clientId);

            return (List<Borrowing>)(List<?>) borrowingList;
        }catch (BorrowingNotFoundException exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }catch (Exception exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
    }

    /**
     * <b>Retourne la liste des emprunts dont le rendu est en retard</b>
     *
     * <p>Permet de récupérer la liste de tous les emprunts non rendu dont la date de rendu est dépassé ordonnées par ordre d'id de client et de date de rendu.</p>
     *
     * @return la liste des emprunts dont le rendu est en retard
     *
     * @throws BorrowingServiceException
     */
    @WebMethod
    public List<Borrowing> getAllBorrowingLate() throws BorrowingServiceException {
        List<BorrowingInterface> borrowinglist;

        try{
            borrowinglist = this.getManagerFactory().getBorrowingManager().getAllLate();

            return (List<Borrowing>)(List<?>) borrowinglist;
        }catch (BorrowingNotFoundException exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }catch (Exception exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
    }

    /**
     * <b>Etend la periode d'emprunt d'un ouvrage</b>
     *
     * <p>Permet d'étendre la periode d'emprunt d'un ouvrage si cela n'a pas déjà été fait. Autrement renvoie une exception.</p>
     *
     * @param borrowingId l'emprunt à prolonger
     *
     * @throws BorrowingServiceException
     */
    @WebMethod
    public void extendBorrowing(Integer borrowingId) throws BorrowingServiceException{
        BorrowingInterface borrowing;
        try{
            borrowing = this.getManagerFactory().getBorrowingManager().getOneById(borrowingId);

            this.getManagerFactory().getBorrowingManager().extend(borrowing);
        }
        catch (ImpossibleExtensionException exception) {
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
        catch (BorrowingNotFoundException exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
        catch (Exception exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
    }

    /**
     * <b>Retourne un ouvrage</b>
     *
     * <p>Web Methode permettant d'éffectuer le retour d'un ouvrage.</p>
     *
     * @param borrowing l'emprunt à rendre
     *
     * @throws BorrowingServiceException
     */
    @WebMethod
    public void returnBook(Borrowing borrowing) throws BorrowingServiceException {
        try{
            this.getManagerFactory().getBorrowingManager().returnBook(borrowing);
        }
        catch (BorrowingNotFoundException exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
        catch (Exception exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
    }

    /**
     * <b>Crée un nouvel emprunt d'ouvrage pour un client</b>
     *
     * <p>Permet de créer un nouvel emprunt pour un client pour un ouvrage donné au moyen du webservice.</p>
     * @param clientId
     * @param bookId
     * @throws BorrowingServiceException
     */
    @WebMethod
    public void borrow(Integer clientId, Integer bookId) throws BorrowingServiceException {
        try{
            this.getManagerFactory().getBorrowingManager().borrow(clientId, bookId);
        } catch (BookUnavailableException exception) {
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        } catch (BookNotFoundException exception) {
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        } catch (ClientNotFoundException exception) {
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        } catch (Exception exception){
            BorrowingServiceFault fault = new BorrowingServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BorrowingServiceException(exception.getMessage(), fault);
        }
    }
}
