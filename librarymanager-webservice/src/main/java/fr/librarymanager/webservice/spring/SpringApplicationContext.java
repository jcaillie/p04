package fr.librarymanager.webservice.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Component;

@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "fr.librarymanager")
@Configuration
public class SpringApplicationContext {

    @Value("${datasource.url}")
    private String url;
    @Value("${datasource.username}")
    private String username;
    @Value("${datasource.password}")
    private String password;

    @Bean
    public DataSource getDataSource(){
         SimpleDriverDataSource dataSource = new SimpleDriverDataSource();

         dataSource.setDriverClass(org.postgresql.Driver.class);
         dataSource.setUrl(this.url);
         dataSource.setUsername(this.username);
         dataSource.setPassword(this.password);

        return dataSource;
    }

    @Bean
    @Autowired
    public DataSourceTransactionManager getTxManager(DataSource dataSource){
        DataSourceTransactionManager txManager = new DataSourceTransactionManager(dataSource);

        return txManager;
    }
}
