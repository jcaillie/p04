package fr.librarymanager.webservice;

import fr.librarymanager.business.manager.ManagerFactory;
import fr.librarymanager.business.manager.ManagerFactoryInterface;
import fr.librarymanager.webservice.spring.SpringApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AbstractService {

    ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringApplicationContext.class);

    @Autowired
    private ManagerFactoryInterface managerFactory = (ManagerFactory) applicationContext.getBean("managerFactory");

    protected ManagerFactoryInterface getManagerFactory(){
        return this.managerFactory;
    }
}
