package fr.librarymanager.webservice.exception;

import javax.xml.ws.WebFault;

@WebFault(name = "BookServiceException")
public class BookServiceException extends Exception {
    private BookServiceFault fault;

    public BookServiceException(String message, BookServiceFault fault){
        super(message);
        this.fault = fault;
    }

    public BookServiceException(String message, Throwable cause, BookServiceFault fault){
        super(message, cause);
        this.fault = fault;
    }

    public BookServiceFault getFaultInfo(){
        return fault;
    }
}
