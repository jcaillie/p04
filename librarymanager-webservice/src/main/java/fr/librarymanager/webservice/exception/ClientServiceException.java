package fr.librarymanager.webservice.exception;

import javax.xml.ws.WebFault;

@WebFault(name = "ClientServiceException")
public class ClientServiceException extends Exception {
    private ClientServiceFault fault;

    public ClientServiceException(String message, ClientServiceFault fault){
        super(message);
        this.fault = fault;
    }

    public ClientServiceException(String message, Throwable cause, ClientServiceFault fault){
        super(message, cause);
        this.fault = fault;
    }

    public ClientServiceFault getFaultInfo(){
        return fault;
    }
}
