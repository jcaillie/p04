package fr.librarymanager.webservice.exception;

import javax.xml.ws.WebFault;

@WebFault
public class BorrowingServiceException extends Exception {
    private BorrowingServiceFault fault;

    public BorrowingServiceException(String message, BorrowingServiceFault fault){
        super(message);
        this.fault = fault;
    }

    public BorrowingServiceException(String message, Throwable cause, BorrowingServiceFault fault){
        super(message, cause);
        this.fault = fault;
    }

    public BorrowingServiceFault getFaultInfo(){
        return fault;
    }
}
