package fr.librarymanager.webservice;

import fr.librarymanager.business.exception.ClientNotFoundException;
import fr.librarymanager.model.Client;
import fr.librarymanager.model.ClientInterface;
import fr.librarymanager.webservice.exception.ClientServiceException;
import fr.librarymanager.webservice.exception.ClientServiceFault;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * <b>WebService gérant la section Client de l'application.</b>
 *
 * @author Jeremy Caillié
 */
@WebService
public class ClientService extends AbstractService {

    private static final Logger log = LogManager.getLogger(ClientService.class);

    @WebMethod
    public Client getClientByEmail(String email) throws ClientServiceException {
        ClientInterface client;

        try{
            client = this.getManagerFactory().getClientManager().getOneByEmail(email);
        }catch (ClientNotFoundException exception){

            ClientServiceFault fault = new ClientServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new ClientServiceException(exception.getMessage(), fault);

        }catch (Exception exception){

            ClientServiceFault fault = new ClientServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new ClientServiceException(exception.getMessage(), fault);
        }

        return (Client) client;
    }

    @WebMethod
    public Client getClientById(Integer id) throws ClientServiceException {
        ClientInterface client;

        try{
            client = this.getManagerFactory().getClientManager().getOneById(id);
        }catch (ClientNotFoundException exception){
            ClientServiceFault fault = new ClientServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new ClientServiceException(exception.getMessage(), fault);
        }catch (Exception exception){

            ClientServiceFault fault = new ClientServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new ClientServiceException(exception.getMessage(), fault);
        }

        return (Client) client;
    }
}
