package fr.librarymanager.webservice;

import fr.librarymanager.business.exception.BookNotFoundException;
import fr.librarymanager.model.Book;
import fr.librarymanager.model.BookInterface;
import fr.librarymanager.webservice.exception.BookServiceException;
import fr.librarymanager.webservice.exception.BookServiceFault;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * <b>Web service du Bean Book.</b>
 * 
 * <p>Web service permettant l'accès au données des ouvrages.</p>
 * 
 * @author Jeremy Caillié
 */
@WebService
public class BookService extends AbstractService{

    private static final Logger log = LogManager.getLogger(BookService.class);

    @WebMethod
    public Book getBookById(Integer id) throws BookServiceException {
        BookInterface book;

        try {
            book = this.getManagerFactory().getBookManager().getOneById(id);

            return (Book) book;

        }catch (BookNotFoundException exception){

            BookServiceFault fault = new BookServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString(exception.getMessage());

            log.info(exception.getMessage());

            throw new BookServiceException(exception.getMessage(), fault);

        }catch (Exception exception){

            BookServiceFault fault = new BookServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BookServiceException(exception.getMessage(), fault);
        }
    }
    
    @WebMethod
    public List<Book> searchBooks(String title, String author, String isbn) throws BookServiceException {
        List<BookInterface> bookList;
        
        try{
            bookList = this.getManagerFactory().getBookManager().search(title, author, isbn);
            
            return (List<Book>)(List<?>) bookList;

        }catch(BookNotFoundException exception){
            BookServiceFault fault = new BookServiceFault();
            fault.setFaultCode("404");
            fault.setFaultString("Aucun ouvrage ne correspond à votre recherche");
            
            throw new BookServiceException(exception.getMessage(), fault);
        }catch(Exception exception){
            BookServiceFault fault = new BookServiceFault();
            fault.setFaultCode("500");
            fault.setFaultString("Erreur du serveur");

            log.error(exception.getMessage());

            throw new BookServiceException(exception.getMessage(), fault);
        }
    }
}
