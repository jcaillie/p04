﻿-- Database: db_p04

-- DROP DATABASE db_p04;

CREATE DATABASE db_p04
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;