/*****************************************************************
*                           Client                               *
*****************************************************************/
\c db_p04;
set datestyle to DMY;
INSERT INTO library_manager.client(
    firstname, lastname, birthday, email, encrypted_password, token
)
VALUES
/***** Client test *****/
(
    'firstnameTest', 'lastnameTest', DATE '1988-01-01', 'test@mymail.fr', '532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25', '532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25'
),
(
    'Jeremy', 'Caillié', DATE '1988-03-25', 'caillie.jeremy@gmail.com', '9ae1f82be2489b48bafa0e579f014274b1251e0bd42d8741d5df242cc5236982', '9ae1f82be2489b48bafa0e579f014274b1251e0bd42d8741d5df242cc5236982'
),
(
    'John', 'Doe', DATE '1975-01-05', 'john.doe@mymail.fr', '63d65bfe030ff5cbaac27bb8c9215bf7b1c635b3a8ed7ee9ad45eccf9e4b2e2f', '63d65bfe030ff5cbaac27bb8c9215bf7b1c635b3a8ed7ee9ad45eccf9e4b2e2f'
);



/*****************************************************************
*                           Books                                *
*****************************************************************/
INSERT INTO library_manager.book (
    title, description,
    author, release_date, isbn, number_of_copies, number_available
)
VALUES
(
    'JavaScript', 'Ce livre sur l''apprentissage du développement avec JavaScript s''adresse à des lecteurs qui souhaitent maîtriser cette brique incontournable des développements Web. En effet, même si des solutions logicielles existent pour contourner la connaissance du langage JavaScript, sa maîtrise est un atout essentiel pour acquérir une expertise dans le domaine des technologies du Web 2.0. En prenant le parti que le lecteur n''a que des connaissances minimales en programmation, l''auteur débute par des rappels ou des apports en algorithmie. Il explique ensuite les bases du langage JavaScript. Les différents concepts, principes ou fonctionnalités sont mis en œuvre au travers d''exemples concrets facilement réutilisables ensuite dans d''autres développements. Dans la mesure où le langage JavaScript interagit avec d''autres technologies Web (ou langages) comme l''incontournable HTML, les feuilles de styles CSS, les langages de script orientés serveurs comme PHP ou des outils comme Ajax, ce livre vous permettra aussi de faire vos premiers pas sur ces différentes technologies. Dans cette seconde édition, le livre intègre de nouveaux chapitres présentant notamment : le framework AngularsJS, les nouveautés ajoutées à JavaScript au travers d''EcmaScript 6, le serveur d''applications Node.js (alternative aux serveurs de type LAMP), le serveur de données Mongo DB (serveur noSQL orienté Big Data). Tous les chapitres du livre intègrent de nombreux exemples largement commentés et en progression logique. Des éléments complémentaires sont en téléchargement sur le site www.editions-eni.fr.',
    'Cédric MILLAURIAUX', DATE '01-02-2018', '978-2-409-01244-0', 3, 2
),
(
    'Symfony 3, Développez des sites web php structurés et performants', 'Ce livre sur Symfony 3 s''adresse aux développeurs, chefs de projets, directeurs techniques, qui souhaitent, grâce à ce framework, structurer et organiser leurs développements PHP au sein d''un cadre de travail robuste et professionnel. La maîtrise de la programmation objet avec PHP est un prérequis indispensable pour tirer le meilleur parti de ces pages. Le livre couvre les principaux composants de Symfony 3, considéré aujourd''hui comme le framework PHP de référence. Il fournit les connaissances de base qui permettent non seulement de créer un site web, mais également de comprendre et maîtriser son fonctionnement en détail. Un chapitre entier est par exemple consacré à l''architecture du framework. Les sujets détaillés vont de l''installation de Symfony à la création de tests unitaires et fonctionnels. La gestion des formulaires, des interactions avec une base de données, des templates, de la journalisation ou même de la sécurité de l''application sont autant de thèmes exposés dans le livre. Le temps de chargement des pages d''un site web étant un élément crucial, un chapitre délivre les techniques et astuces pour fortement améliorer les performances d''une application. L''auteur a structuré les chapitres pour faciliter l''apprentissage de Symfony 3 et dévoile au fil des pages des conseils, bonnes pratiques et exemples détaillés. Des éléments complémentaires sont en téléchargement sur le site www.editions-eni.fr.',
    'Bilal AMARNI', DATE '01-01-2018', '978-2-409-01172-6', 2, 2
),
(
    'SQL, Les fondamentaux du langage', 'Ce livre sur les fondamentaux du langage SQL s''adresse aux développeurs et informaticiens débutants appelés à travailler avec un Système de Gestion de Bases de Données Relationnelles (SGBDR) pour stocker et manipuler des données. Son objectif est de décrire les ordres principaux les plus utilisés du langage SQL (indépendamment des déclinaisons réalisées par les éditeurs de SGBDR) pour permettre au lecteur de prendre en main rapidement une base de données relationnelle et être capable de créer des tables, de les interroger, de les modifier, d''insérer et de supprimer des lignes. Le livre débute par un bref historique sur la création de la norme SQL puis présente quelques notions sur le modèle relationnel. Ensuite, chaque chapitre présente une subdivision de SQL : la création et la manipulation des tables puis la gestion des données dans ces tables en incluant les dernières évolutions comme les fonctions de fenêtrage. L''auteur enchaîne avec la sécurité des données et quelques notions de transactions, puis présente la programmation avec quelques éléments de PL/SQL et l''étude des déclencheurs. Le livre se termine en abordant des thèmes un peu plus complexes comme les chargements en masse, les imports et exports de tables, les notions de performances ou encore les objets systèmes.',
    'Eric GODOC', DATE '01-12-2017', '978-2-409-01142-9', 1, 0
),
(
    'Manuel de Mathématiques', 'Sans artifice, ce manuel de mathématiques regroupe la totalité des connaissances obligatoires de terminale S et y ajoute des outils pour aller plus loin. Il s''appuie sur quatre piliers éprouvés par l''expérience de l''enseignement : un recueil de techniques de base où les habitudes mathématiques fondamentales (calculs élémentaires, notations, rédaction et logique) sont détaillées sans implicite. un cours complet, respectueux du programme, étoffé de conseils et de commentaires off. Il propose un enchaînement des notions progressif et vivant où chaque point est démontré avec rigueur. plus de 500 exercices à travailler, tous avec une solution soigneusement rédigée. Que ce soit pour s''entraîner, illustrer le cours, détailler un classique ou exposer une méthode, chaque exercice répond à un objectif spécifique. des compléments et développements riches s''appuyant sur les seules connaissances de terminale. Les points explorés entrouvrent de façon constructive les portes de l''enseignement supérieur. L''ouvrage s''adresse à tous les élèves de lycée, notamment ceux qui ambitionnent de se diriger dans une filière où les mathématiques ont une place conséquente. Il pourra intéresser les étudiants de classe préparatoire aux grandes écoles, les candidats aux CAPES et agrégation et les professeurs curieux de voir jusqu''où un cours approfondi de terminale peut être poussé.',
    'Emmanuel LEVET', DATE '01-09-2018', '978-2-340-02381-9', 2, 2
),
(
    'L''existentialisme est un humanisme', '«L''existentialisme n''est pas autre chose qu''un effort pour tirer toutes les conséquences d''une position athée cohérente. Elle ne cherche pas du tout à plonger l''homme dans le désespoir. Mais si l''on appelle, comme les chrétiens, désespoir toute attitude d''incroyance, elle part du désespoir originel. L''existentialisme n''est pas tellement un athéisme au sens où il s''épuiserait à démontrer que Dieu n''existe pas. Il déclare plutôt : même si Dieu existait, ça ne changerait rien ; voilà notre point de vue. Non pas que nous croyions que Dieu existe, mais nous pensons que le problème n''est pas celui de son existence ; il faut que l''homme se retrouve lui-même et se persuade que rien ne peut le sauver de lui-même, fût-ce une preuve valable de l''existence de Dieu. En ce sens, l''existentialisme est un optimisme, une doctrine d''action.»',
    'Jean-Paul SARTRE', DATE '23-01-1996', '9782070329137', 4, 3
);




/*****************************************************************
*                           Emprunts                             *
*****************************************************************/
INSERT INTO library_manager.borrowing (
    borrowing_date, deadline_for_rendering, extended, rendered_up, client_id, book_id
)
VALUES
(
    DATE '5-03-2018', DATE '2-04-2018', FALSE, null, (SELECT id FROM library_manager.client WHERE email = 'caillie.jeremy@gmail.com'), (SELECT id FROM library_manager.book WHERE title = 'JavaScript')
),
(
    DATE '5-03-2018', DATE '2-04-2018', FALSE, null, (SELECT id FROM library_manager.client WHERE email = 'caillie.jeremy@gmail.com'), (SELECT id FROM library_manager.book WHERE title = 'SQL, Les fondamentaux du langage')
),
(
    DATE '26-02-2018', DATE '26-03-2018', FALSE, DATE '23-03-2018', (SELECT id FROM library_manager.client WHERE email = 'john.doe@mymail.fr'), (SELECT id FROM library_manager.book WHERE title = 'Manuel de Mathématiques')
),
(
    DATE '26-02-2018', DATE '23-04-2018', TRUE, null, (SELECT id FROM library_manager.client WHERE email = 'john.doe@mymail.fr'), (SELECT id FROM library_manager.book WHERE title = 'Manuel de Mathématiques')
);