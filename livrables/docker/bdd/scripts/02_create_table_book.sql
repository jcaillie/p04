﻿-- Table: library_manager.book

-- DROP TABLE library_manager.book;
\c db_p04;
CREATE TABLE library_manager.book
(
  id serial NOT NULL,
  title character varying(256) NOT NULL,
  description text NOT NULL,
  author character varying(50) NOT NULL,
  release_date date NOT NULL,
  isbn character varying(50) NOT NULL,
  number_of_copies integer NOT NULL,
  number_available integer NOT NULL,
  CONSTRAINT book_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE library_manager.book
  OWNER TO postgres;

-- Index: library_manager.book_id_uindex

-- DROP INDEX library_manager.book_id_uindex;

CREATE UNIQUE INDEX book_id_uindex
  ON library_manager.book
  USING btree
  (id);

-- Index: library_manager.book_isbn_uindex

-- DROP INDEX library_manager.book_isbn_uindex;

CREATE UNIQUE INDEX book_isbn_uindex
  ON library_manager.book
  USING btree
  (isbn COLLATE pg_catalog."default");

-- Index: library_manager.book_title_uindex

-- DROP INDEX library_manager.book_title_uindex;

CREATE UNIQUE INDEX book_title_uindex
  ON library_manager.book
  USING btree
  (title COLLATE pg_catalog."default");