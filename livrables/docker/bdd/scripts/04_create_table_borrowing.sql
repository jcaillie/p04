﻿-- Table: library_manager.borrowing

-- DROP TABLE library_manager.borrowing;
\c db_p04;
CREATE TABLE library_manager.borrowing
(
  "number" serial NOT NULL,
  borrowing_date date NOT NULL,
  deadline_for_rendering date NOT NULL,
  extended boolean NOT NULL DEFAULT false,
  rendered_up date,
  client_id integer NOT NULL,
  book_id integer NOT NULL,
  CONSTRAINT borrowing_pkey PRIMARY KEY (number),
  CONSTRAINT borrowing_book_id_fk FOREIGN KEY (book_id)
      REFERENCES library_manager.book (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT borrowing_client_id_fk FOREIGN KEY (client_id)
      REFERENCES library_manager.client (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE library_manager.borrowing
  OWNER TO postgres;

-- Index: library_manager.borrowing_number_uindex

-- DROP INDEX library_manager.borrowing_number_uindex;

CREATE UNIQUE INDEX borrowing_number_uindex
  ON library_manager.borrowing
  USING btree
  (number);

-- Index: library_manager.fki_borrowing_book_id_fk

-- DROP INDEX library_manager.fki_borrowing_book_id_fk;

CREATE INDEX fki_borrowing_book_id_fk
  ON library_manager.borrowing
  USING btree
  (book_id);

-- Index: library_manager.fki_borrowing_client_id_fk

-- DROP INDEX library_manager.fki_borrowing_client_id_fk;

CREATE INDEX fki_borrowing_client_id_fk
  ON library_manager.borrowing
  USING btree
  (client_id);

