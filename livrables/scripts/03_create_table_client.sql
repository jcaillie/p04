﻿-- Table: library_manager.client

-- DROP TABLE library_manager.client;
\c db_p04;
CREATE TABLE library_manager.client
(
  id serial NOT NULL,
  firstname character varying(25) NOT NULL,
  lastname character varying(25) NOT NULL,
  birthday date NOT NULL,
  email character varying(36) NOT NULL,
  encrypted_password character varying(256) NOT NULL,
  token character varying(256) NOT NULL,
  CONSTRAINT client_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE library_manager.client
  OWNER TO postgres;

-- Index: library_manager.client_email_uindex

-- DROP INDEX library_manager.client_email_uindex;

CREATE UNIQUE INDEX client_email_uindex
  ON library_manager.client
  USING btree
  (email COLLATE pg_catalog."default");

-- Index: library_manager.client_id_uindex

-- DROP INDEX library_manager.client_id_uindex;

CREATE UNIQUE INDEX client_id_uindex
  ON library_manager.client
  USING btree
  (id);

-- Index: library_manager.client_token_uindex

-- DROP INDEX library_manager.client_token_uindex;

CREATE UNIQUE INDEX client_token_uindex
  ON library_manager.client
  USING btree
  (token COLLATE pg_catalog."default");

